package com.embraser01.android.ultimatewol.list;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Marc-Antoine on 13/07/2015.
 */

public class ListPC {

    private static final String TABLE_PC = BaseSQL.TABLE_PC;

    private static final String COL_ID = BaseSQL.COL_ID;
    private static final String COL_NAME = BaseSQL.COL_NAME;
    private static final String COL_MAC = BaseSQL.COL_MAC;
    private static final String COL_IP = BaseSQL.COL_IP;
    private static final String COL_PORT = BaseSQL.COL_PORT;
    private static final String COL_LASTUSED = BaseSQL.COL_LASTUSED;
    private static final String COL_USED_CNT = BaseSQL.COL_USED_CNT;


    private static final int NUM_COL_ID = BaseSQL.NUM_COL_ID;
    private static final int NUM_COL_NAME = BaseSQL.NUM_COL_NAME;
    private static final int NUM_COL_MAC = BaseSQL.NUM_COL_MAC;
    private static final int NUM_COL_IP = BaseSQL.NUM_COL_IP;
    private static final int NUM_COL_PORT = BaseSQL.NUM_COL_PORT;
    private static final int NUM_COL_LASTUSED = BaseSQL.NUM_COL_LASTUSED;
    private static final int NUM_COL_USED_CNT = BaseSQL.NUM_COL_USED_CNT;


    private static SQLiteDatabase bdd;

    private BaseSQL myBaseSQL;


    // Initialisation

    public ListPC(Context context) {
        // Creation - Chargement de la bdd + table
        myBaseSQL = new BaseSQL(context, "ListPC.db", null);
    }


    // Ouverture et fermeture

    public void open() {
        //ouverture de la BDD pour l'ecriture
        bdd = myBaseSQL.getWritableDatabase();
    }

    public void close() {
        //fermeture de la BDD pour l'ecriture
        bdd.close();
    }



    // Accesseurs

    public SQLiteDatabase getBDD() {
        return bdd;
    }

    public int getCount(){
        return bdd.rawQuery("SELECT " + COL_ID + " FROM " + TABLE_PC, null).getCount();
    }


    // Add new PC

    public long addPC(PC pc) {

        ContentValues values = new ContentValues();

        values.put(COL_NAME, pc.getName());
        values.put(COL_MAC, pc.getMac());
        values.put(COL_IP, pc.getIp());
        values.put(COL_PORT, pc.getPort());
        values.put(COL_LASTUSED, pc.getLast_used());
        values.put(COL_USED_CNT, 0);

        return bdd.insert(TABLE_PC, null, values);
    }


    // Update PC

    public int updatePC(PC oldPC, PC newPC) {

        ContentValues values = new ContentValues();
        values.put(COL_NAME, newPC.getName());
        values.put(COL_MAC, newPC.getMac());
        values.put(COL_IP, newPC.getIp());
        values.put(COL_PORT, newPC.getPort());
        values.put(COL_LASTUSED, newPC.getLast_used());
        values.put(COL_USED_CNT, newPC.getUsed_cnt());

        return bdd.update(TABLE_PC, values, COL_ID + " = " + oldPC.getId(), null);
    }


    // Delete PC

    public int deletePC(PC pc) {
        return bdd.delete(TABLE_PC, COL_ID + " = " + pc.getId(),  null);
    }


    // Select all PC

    public ArrayList<PC> getPC(){
        if(this.getCount() == 0)
            return new ArrayList<>();

        ArrayList<PC> pcList = new ArrayList<>();

        Cursor c = bdd.rawQuery("SELECT *" + " FROM " + TABLE_PC, null);
        c.moveToFirst();


        while(!c.isAfterLast()){
            pcList.add(new PC(c.getInt(NUM_COL_ID),
                    c.getString(NUM_COL_NAME),
                    c.getString(NUM_COL_MAC),
                    c.getString(NUM_COL_IP),
                    c.getString(NUM_COL_PORT),
                    c.getString(NUM_COL_LASTUSED),
                    c.getInt(NUM_COL_USED_CNT)));
            c.moveToNext();
        }

        return pcList;
    }


    // Select one PC

    public PC selectPC(int id) {

        if(this.getCount() == 0)
            return null;

        Cursor c = bdd.rawQuery("SELECT *" + " FROM " + TABLE_PC + " WHERE _id = '" + id + "'", null);

        if(c.getCount() == 0)
            return null;

        c.moveToFirst();

        return new PC(c.getInt(NUM_COL_ID),
                c.getString(NUM_COL_NAME),
                c.getString(NUM_COL_MAC),
                c.getString(NUM_COL_IP),
                c.getString(NUM_COL_PORT),
                c.getString(NUM_COL_LASTUSED),
                c.getInt(NUM_COL_USED_CNT));
    }
}
