package com.embraser01.android.ultimatewol.list;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.embraser01.android.ultimatewol.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Marc-Antoine on 14/07/2015.
 */
public class ListPCAdapter extends RecyclerView.Adapter<ListPCAdapter.PCViewHolder> {

    public static final int BY_DEFAULT = 0;
    public static final int BY_NAME = 1;
    public static final int BY_LAST_USED = 2;
    public static final int BY_USED_COUNT = 3;


    private ArrayList<PC> pcArrayList;
    private SparseBooleanArray selectedPC;

    public ListPCAdapter(ArrayList<PC> pcArrayList){
        this.pcArrayList = pcArrayList;
        this.selectedPC = new SparseBooleanArray();
    }


    @Override
    public PCViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.pc_card, parent, false);
        return new PCViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PCViewHolder holder, int position) {
        holder.pc_name.setText(pcArrayList.get(position).getName());
        holder.pc_mac.setText(pcArrayList.get(position).getMac());
        holder.pc_ip.setText(pcArrayList.get(position).getIp());
    }

    @Override
    public int getItemCount() {
        return pcArrayList.size();
    }


    public void reorderBy(int ordermode){
        Collections.sort(pcArrayList,new PCComparator(ordermode));
        this.notifyDataSetChanged();
    }


    public class PCViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private CardView mCardView;
        private TextView pc_name;
        private TextView pc_mac;
        private TextView pc_ip;


        public PCViewHolder(View itemView) {
            super(itemView);

            mCardView = (CardView) itemView.findViewById(R.id.cardview);
            pc_name = (TextView) itemView.findViewById(R.id.pc_name);
            pc_mac = (TextView) itemView.findViewById(R.id.pc_mac);
            pc_ip = (TextView) itemView.findViewById(R.id.pc_ip);


            mCardView.setOnLongClickListener(this);
            mCardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Snackbar.make(view.getRootView(),"onClick",Snackbar.LENGTH_SHORT).show();

        }

        @Override
        public boolean onLongClick(View view) {
            Snackbar.make(view.getRootView(),"LongClick",Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }



    public class PCComparator implements Comparator<PC>  {

        private int ordermode;

        public PCComparator(int ordermode) {
            this.ordermode = ordermode;
        }

        @Override
        public int compare(PC pc1, PC pc2) {
            int result = 0;
            switch(ordermode){
                case ListPCAdapter.BY_DEFAULT:
                    if(pc1.getId() < pc2.getId())
                        result = -1;
                    else
                        result = 1;
                    break;
                case ListPCAdapter.BY_NAME:
                    result = pc1.getName().compareTo(pc2.getName());
                    break;
                case ListPCAdapter.BY_LAST_USED:
                    result = (-1) * pc1.getLast_used().compareTo(pc2.getLast_used());
                    break;
                case ListPCAdapter.BY_USED_COUNT:
                    if(pc1.getUsed_cnt() < pc2.getUsed_cnt())
                        result = 1;
                    else
                        result = -1;
                    break;
            }

            return result;
        }
    }
}
