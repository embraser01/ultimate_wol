package com.embraser01.android.ultimatewol.list;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.embraser01.android.ultimatewol.R;

import java.util.ArrayList;

/**
 * Created by Marc-Antoine on 13/07/2015.
 */
public class ListPCFragment extends Fragment {

    public static final String TAG = "ListPCFragment";

    private RecyclerView mRecyclerView;
    private ListPCAdapter mListPCAdpater;

    private FloatingActionButton mFab;


    private ListPC mListPC;
    private ArrayList<PC> pcArrayList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_listpc,container,false);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        /* FloatingActionButton */

        mFab = (FloatingActionButton) this.getView().findViewById(R.id.fab);
        mFab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Snackbar.make(v,"FAB Clicked",Snackbar.LENGTH_SHORT).show();
            }
        });


        /* Database & RecyclerView (Adapter + LayoutManager) */

        mListPC = new ListPC(getActivity());
        mListPC.open();
        pcArrayList = mListPC.getPC();
        mListPCAdpater = new ListPCAdapter(pcArrayList);

        mRecyclerView = (RecyclerView) this.getView().findViewById(R.id.recyclerview);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mListPCAdpater);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mListPC.close();
    }
}